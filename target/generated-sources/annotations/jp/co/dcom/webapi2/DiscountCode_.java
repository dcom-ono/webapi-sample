package jp.co.dcom.webapi2;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jp.co.dcom.webapi2.Customer;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-07-07T20:49:39")
@StaticMetamodel(DiscountCode.class)
public class DiscountCode_ { 

    public static volatile SingularAttribute<DiscountCode, BigDecimal> rate;
    public static volatile CollectionAttribute<DiscountCode, Customer> customerCollection;
    public static volatile SingularAttribute<DiscountCode, String> discountCode;

}